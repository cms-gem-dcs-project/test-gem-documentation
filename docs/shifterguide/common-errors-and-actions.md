# What does DCS mean?

![binbash-leverage-tf](../assets/images/shifterguide/DCS_image.png#right "GEm_DCS"){: style="width:320px"}

!!! note "DCS = Detector Control System"

Detector is too complex and it not possible to operate a detector by a small crew of experts. 
A Detector has to run without interruption for many years, except long shutdown periods.
In order to make it possible to operate this detector easily, the name of detector control system (DCS) will come to the picture. 

DCS system of the GEM GE1/1 station has been developed based on a Supervisory Control and Data Acquisition (SCADA) software call **"Wincc OA"**.

!!! check "DCS Purpose"
    * [x] **Continuous and safe operation of the detectors** 
    * [x] **Voltage (HV/LV) controlling and monitoring**
	* [x] **Monitoring of gas system and environmental parameters**
	* [x] **External system communication**

!!! info "The GEM DCS actually consists of 5 main sections:"
    * **Standard HV + LV control and monitoring** 
    * **Finite State Machine (FSM)**
	* **Gas and environmental parameters monitoring**
	* **Radiation monitoring (RadMon)**
	* **LHC, Magnet, DSS, Temperature monitoring**

The purpose of this document is to provide an integrated and complete documentation source required for an operator.
