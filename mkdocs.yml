#===================================================================================#
# Site Information
#===================================================================================#
site_name: CMS GEM DCS Documentation
site_description: This is the Documentation website of the CMS GEM Detector Controlling System.
site_author: CERN CMS GEM DCS Team
site_url: https://cmsgemdcs.web.cern.ch/

repo_name: gem-dcs-documentation
repo_url: https://gitlab.cern.ch/cms-gem-dcs-project/gem-dcs-documentation.git
edit_uri: "https://gitlab.cern.ch/cms-gem-dcs-project/gem-dcs-documentation.git"

# Copyright
copyright: Copyright &copy; 2020 - 2021 GEM DCS Team
#===================================================================================#
# Mkdocs Theme (Material Mkdocs) + Extras
#===================================================================================#
# Configuration
theme:
  name: material
  custom_dir: material

  # Static files
  static_templates:
    - 404.html

  # Don't include MkDocs' JavaScript
  include_search_page: false
  search_index_only: true

  icon: # https://www.materialpalette.com/icons
    logo: material/book-open-page-variant
  favicon: assets/images/logos/favicon.ico
  font:
    text: Roboto
    code: Roboto Mono
    
  # Default values, taken from mkdocs_theme.yml
  language: en
  features:
    - navigation.tabs
    - navigation.instant
    - navigation.top
    - header.autohide
    #- navigation.sections
  palette: # https://www.materialpalette.com/colors 
    - scheme: preference
      #primary: indigo # comment to replace by /docs/assets/stylesheets
      accent: deep purple
      toggle:
        icon: material/weather-night    #material/toggle-switch-off-outline 
        name: Switch to dark mode
    - scheme: slate
      primary: indigo # comment to replace by /docs/assets/stylesheets
      accent: cyan
      toggle:
        icon: material/weather-sunny    #material/toggle-switch  
        name: Switch to light mode

#===================================================================================#
# Mkdocs Markdown Extentions, Plugins & extras
#===================================================================================#
markdown_extensions:
  - admonition
  - abbr
  - attr_list
  - def_list
  - footnotes
  - meta
  - md_in_html
  - codehilite
  - toc:
      permalink: true
      # insert a blank space before the character
      #permalink: " ¶"
  - pymdownx.arithmatex:
      generic: true
  - pymdownx.betterem:
      smart_enable: all
  - pymdownx.caret
  - pymdownx.critic
  - pymdownx.details
  - pymdownx.emoji:
      emoji_index: !!python/name:materialx.emoji.twemoji
      emoji_generator: !!python/name:materialx.emoji.to_svg
  - pymdownx.highlight
  - pymdownx.inlinehilite
  - pymdownx.keys
  - pymdownx.magiclink:
      repo_url_shorthand: true
      user: squidfunk
      repo: mkdocs-material
  - pymdownx.mark
  - pymdownx.smartsymbols
  - pymdownx.superfences:
      custom_fences:
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  - pymdownx.tabbed
  - pymdownx.tasklist:
      custom_checkbox: true
  - pymdownx.tilde
  
# Plugins
plugins:
  - search
  - git-revision-date-localized:  #https://how-to.docs.cern.ch/advanced/automatic_date/
        type: iso_datetime          # and https://timvink.github.io/mkdocs-git-revision-date-localized-plugin/options/
        enable_creation_date: true
  - git-authors
#  - git-revision-date
#  - awesome-pages
  - minify:
      minify_html: true

extra_css:
    - assets/stylesheets/extra.css
    
#===================================================================================#
# Mkdocs Navigation Tree
#===================================================================================#
nav:
    - Home: index.md
    - Shifter's Guide:
        - Introduction:
            - Introduction: shifterguide/introduction.md
            - Getting Started: shifterguide/getting-started.md
        - General Infromation:
            - Nomenclature of GEM detector: shifterguide/nomenclature.md
            - How to connect to DCS: shifterguide/how-to-connect-to-DCS.md
        - How to put FSM in central and local: shifterguide/put-fsm-central.md
        - How to react to an alarm: shifterguide/how-to-react-to-alarm.md
        - List of common errors and actions to be taken: shifterguide/common-errors-and-actions.md
    - Expert's Guide:
        - Overview: expertguide/index.md
        - Special Topics:
            - How to login to the mainframe remotely: expertguide/login_mainframe_remotely.md
            - How to power on GEM detector after a shutdown: expertguide/poweron-detector-after-shutdown.md
    - Developer's Guide:
        - Overview: devguide/index.md
        - Special Topics:
            - How to add DCS component in Component Handler: 
                - Adding-new-component: devguide/adding-new-component.md
                - Updating-existing-component: devguide/update-existing-componet.md
            - How to use db editor and navigator tool: devguide/db-editor-and-navigator-tool.md
            - How to create new configurations in cofigDb: devguide/new-config-in-confDb.md
            - How to generate DP list files: devguide/generate-dp-lists.md
            - How to register a new mainframe: devguide/regisering_new_mainframe.md  
        - DCS Documentation: 
            - How it works: devguide/documentation/how-it-works.md
            - How to edit pages: devguide/documentation/how-to-edit-pages.md
    - FAQ: FAQ/index.md
    - Contact: contact.md

# Google Analytics (https://squidfunk.github.io/mkdocs-material/setup/setting-up-site-analytics/)
#
google_analytics:
  - !!python/object/apply:os.getenv ["GOOGLE_ANALYTICS_KEY"]
  - auto
